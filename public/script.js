/* Reaction Game (JS) - Version 1.2
 * Measures the users reaction time.
 * Copyright (C) 2021  Vincent Rietz
 *   
 * This program is licensed under the MIT License. Check the LICENSE file 
 * for more information or visit <https://opensource.org/licenses/MIT>.
 */

let timer = 0;

let timerInterval;
let textInterval;

let startTimeout;

let currentScreen = -1;


function showOverlay() {
    document.getElementById("overlay").style.display = "block";
}

function hideOverlay() {
    document.getElementById("overlay").style.display = "none";
}

function isOverlayAction(event) {
    let target = event.target;
    return (target.id == "instructions_link" || document.getElementById("overlay").contains(target));
}

function setColors(gameStarted) {
    if (gameStarted) {
        document.body.style.color = "#3B3E48";
        document.body.style.backgroundColor = "#DDB817";
        document.body.style.caretColor = "#DDB817";
    } else {
        document.body.style.color = "#DDB817";
        document.body.style.backgroundColor = "#3B3E48";
        document.body.style.caretColor = "#3B3E48";
    }
}

function updateTimer() {
    timer++;
    document.getElementById("text1").innerHTML = (timer / 1000).toFixed(3) + "&#8239;s";
}

function updateText() {
    document.getElementById("text1").innerHTML += ".";
}

function keyPressed(event) {
    if (!isValidEvent(event)) {
        return;
    }
    
    event.preventDefault();
    switch (currentScreen) {
        case 0: 
            startTimeout = window.setTimeout(showTimerScreen, Math.floor((Math.random() * 7000)) + 1000);
            showWaitScreen();
            break;
        case 1:
            window.clearTimeout(startTimeout);
            window.clearInterval(textInterval);
            showEndScreen(false);
            break;
        case 2:
            window.clearInterval(timerInterval);
            showEndScreen(true);
            break;
        case 3:
            showStartScreen();
            break;
        default: break;
    }
}

function isValidEvent(event) {
    if (isOverlayAction(event)) {
        return false;
    }
    
    if (event.type == "mousedown") {
        return (event.button == 0);
    } else if (event.type == "keydown") {
        return (event.code == "Space");
    }
    
    return false;
}

function showStartScreen() {
    currentScreen = 0;
    timer = 0;
    
    setColors(false);
    
    document.getElementById("text1").innerHTML = "Test your reaction time.";
    document.getElementById("text2").innerHTML = "Press spacebar, click or touch to start.";
    document.getElementById("text3").innerHTML = "As soon as the colors change, press again and check your score.";
}

function showWaitScreen() {
    currentScreen = 1;
    textInterval = window.setInterval(updateText, 1000);
    
    document.getElementById("text1").innerHTML = "Wait.";
    document.getElementById("text2").innerHTML = "...for the colors to change.";
    document.getElementById("text3").innerHTML = "&nbsp;";
}

function showTimerScreen() {
    currentScreen = 2;
    window.clearInterval(textInterval);
    timerInterval = window.setInterval(updateTimer, 1);
    
    setColors(true);
    
    document.getElementById("text2").innerHTML = "Press spacebar, click or touch NOW!";
    document.getElementById("text3").innerHTML = "&nbsp;";
}

function showEndScreen(gameStarted) {
    currentScreen = 3;
    
    if (!gameStarted) {
        document.getElementById("text1").innerHTML = "Pressed to early!";
        document.getElementById("text2").innerHTML = "You lost.";
    } else {
        document.getElementById("text2").innerHTML = "Can you do better?";
    }
    
    document.getElementById("text3").innerHTML = "Press spacebar, click or touch to go back to start.";
}

document.addEventListener("keydown", keyPressed, {passive: false});
document.body.addEventListener("mousedown", keyPressed, {passive: false});
document.body.addEventListener("touchstart", keyPressed, {passive: false});

showStartScreen();
