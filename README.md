# Reaction Game
Small HTML/CSS/JavaScript game to measure and show the users reaction time.

## Usage
The project is available online under [vrietz.gitlab.io/reaction/](https://vrietz.gitlab.io/reaction/).

For offline use, download the project and open `public/index.html` in a browser.

## License 
The project is published under the MIT License.
